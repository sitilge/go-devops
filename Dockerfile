FROM ubuntu:latest

ENV PATH="${PATH}:/root/go/bin"

ARG DEBIAN_FRONTEND=noninteractive

#Generic install
RUN apt-get update && apt-get install -y \
    apt-transport-https \
    build-essential \
    curl \
    git \
    gnupg \
    golang \
    jq \
    rename \
    software-properties-common \
    ssh \
    unzip \
    && rm -rf /var/lib/apt/lists/*

#AWS CLI install
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip && ./aws/install && rm awscliv2.zip

#Kubectl install
RUN curl -sLO "https://dl.k8s.io/release/$(curl -sL https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN curl -sLO "https://dl.k8s.io/$(curl -sL https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
RUN echo "$(cat kubectl.sha256) kubectl" | sha256sum --check
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

#Helm preparation
RUN curl -sL https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg > /dev/null
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list

#Terraform preparation
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

#Helm and Terraform install
RUN apt-get update && apt-get install -y \
    helm \
    terraform \
    && rm -rf /var/lib/apt/lists/*
