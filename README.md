# go-devops

A Dockerfile for building an image based on Ubuntu with some DevOps tools. This image contains:

- `awscli`
- `curl`
- `bash`
- `git`  
- `go`
- `helm`
- `jq`
- `kubectl`
- `ssh`
- `terraform`

## Security

This image should be not be used as the final image, merely as an intermediate one. For improved security, please consider the following snippet:

```Dockerfile
#Some advice from: https://github.com/hexops/dockerfile
#Do not run as root, use a non-existing user and group with UIDs
#larger than 10000 so the new user does not accidentally end up
#with more privilges.
ARG name=nonroot
ARG id=12345

RUN addgroup -g ${id} -S ${name} && adduser -u ${id} -S -G ${name} -h /home/${name} ${name}

#Switch to the new user.
USER ${name}
```

## Building

Build the image:

````
docker build -t sitilge/go-devops:latest .
````

## Pushing

Push the image to the Docker Hub:

````
docker build -t sitilge/go-devops:latest .

docker login
docker push sitilge/go-devops
````

## CI/CD

Set up the following values in GitLab CI/CD variables section:

- `CI_REGISTRY_USER` - Docker Hub username.
- `CI_REGISTRY_PASSWORD` - Docker Hub access token or password (not recommended).
